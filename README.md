go install gitlab.com/softkot/rustore-cli@latest

Simple CLI to upload APK to RuStore API

Usage of rustore-cli:

-key-id string Key id. Default from env RUSTORE_KEY_ID

-key-file string PEM encoded RSA secret key file. Default from env RUSTORE_KEY

-aab string aab file

-main-apk string main apk file

-main-hms-apk string main HMS apk file

-package string application package name. Default from env APP_PACKAGE_NAME

-publish-type string publish type (default "MANUAL")

-secondary-apk string secondary apk file signed other than main

-secondary-hms-apk string secondary HMS apk file signed other than main

