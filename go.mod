module gitlab.com/softkot/rustore-cli

go 1.21

require github.com/go-resty/resty/v2 v2.14.0

require golang.org/x/net v0.28.0 // indirect
