package main

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"flag"
	"fmt"
	"github.com/go-resty/resty/v2"
	"io"
	"os"
	"regexp"
	"strconv"
	"time"
)

var keyId = flag.String("key-id", os.Getenv("RUSTORE_KEY_ID"), "Key id. Default from env RUSTORE_KEY_ID")
var rustoreKeyFile = flag.String("key-file", os.Getenv("RUSTORE_KEY"), "PEM encoded RSA secret key file. Default from env RUSTORE_KEY")
var packageName = flag.String("package", os.Getenv("APP_PACKAGE_NAME"), "application package name. Default from env APP_PACKAGE_NAME")
var publishType = flag.String("publish-type", "MANUAL", "publish type")
var mainApk = flag.String("main-apk", "", "main apk file")
var aab = flag.String("aab", "", "aab file")
var secondaryApk = flag.String("secondary-apk", "", "secondary apk file signed other than main")
var mainHmsApk = flag.String("main-hms-apk", "", "main HMS apk file")
var secondaryHmsApk = flag.String("secondary-hms-apk", "", "secondary HMS apk file signed other than main")
var debugMode = flag.Bool("debug", false, "debug API")

var rustoreApi = resty.New()

type RustoreCommonReply struct {
	Code      string    `json:"code"`
	Message   string    `json:"message"`
	Timestamp time.Time `json:"timestamp"`
}

type RustoreJwtReply struct {
	RustoreCommonReply
	Body struct {
		Jwe string `json:"jwe"`
		Ttl int    `json:"ttl"`
	} `json:"body"`
}

type RustoreVersionReply struct {
	RustoreCommonReply
	Version int64 `json:"body"`
}

type ApkAttributes struct {
	Filename    string
	ServiceType string
	IsMain      bool
	IsAAB       bool
}

var jwe string
var draftVersion int64
var apks = make([]ApkAttributes, 0)

func main() {
	flag.Parse()
	rustoreApi.SetDebug(*debugMode)
	/*	rustoreApi.OnError(func(request *resty.Request, err error) {
			fmt.Printf("rustore REST api error %v\n%v\n", request.URL, err)
		})
	*/
	rustoreApi.SetDisableWarn(true)
	rustoreApi.SetRetryCount(3)
	rustoreApi.SetHeaders(map[string]string{
		"Content-Type": "application/json",
		"User-Agent":   "rustore-cli/1.0",
	})
	rustoreApi.OnBeforeRequest(func(client *resty.Client, request *resty.Request) error {
		request.SetHeader("Public-Token", jwe)
		return nil
	})
	rustoreApi.OnRequestLog(func(log *resty.RequestLog) error {
		if len(log.Body) > 4096 {
			log.Body = "Log too long to display...."
		}
		return nil
	})
	rustoreApi.OnResponseLog(func(log *resty.ResponseLog) error {
		if len(log.Body) > 4096 {
			log.Body = "Log too long to display...."
		}
		return nil
	})
	existentVersion := regexp.MustCompile("draft version[^0-9]+([0-9]+)")
	rustoreApi.OnAfterResponse(func(client *resty.Client, response *resty.Response) error {
		reply := &RustoreCommonReply{}
		if err := json.Unmarshal(response.Body(), reply); err == nil {
			if reply.Code == "ERROR" && reply.Message != "" {
				if vr, ok := response.Result().(*RustoreVersionReply); ok {
					if match := existentVersion.FindStringSubmatch(reply.Message); len(match) == 2 {
						if v, err := strconv.ParseInt(match[1], 10, 64); err == nil {
							vr.Version = v
							return nil
						}
					}
				}
				return errors.New(reply.Message)
			}
		}

		if err, ok := response.Error().(error); ok {
			return err
		}
		if response.IsError() {
			return errors.New(string(response.Body()))
		}
		return nil
	})
	if *mainApk != "" {
		apks = append(apks, ApkAttributes{
			Filename:    *mainApk,
			IsMain:      true,
			ServiceType: "Unknown",
		})
	}
	if *secondaryApk != "" {
		apks = append(apks, ApkAttributes{
			Filename:    *secondaryApk,
			IsMain:      false,
			ServiceType: "Unknown",
		})
	}
	if *mainHmsApk != "" {
		apks = append(apks, ApkAttributes{
			Filename:    *mainApk,
			IsMain:      true,
			ServiceType: "HMS",
		})
	}
	if *secondaryHmsApk != "" {
		apks = append(apks, ApkAttributes{
			Filename:    *secondaryHmsApk,
			IsMain:      false,
			ServiceType: "HMS",
		})
	}
	if *aab != "" {
		apks = append(apks, ApkAttributes{
			Filename: *aab,
			IsAAB:    true,
		})
	}

	if *rustoreKeyFile == "" || *keyId == "" || *packageName == "" || len(apks) == 0 {
		flag.Usage()
		return
	}
	if privateKeyFile, err := os.Open(*rustoreKeyFile); err != nil {
		panic(err)
	} else {
		if keydata, err := io.ReadAll(privateKeyFile); err != nil {
			panic(err)
		} else {
			privateKeyData, _ := pem.Decode(keydata)
			if privateKeyData == nil {
				panic("invalid key data")
			}
			if rustorePrivateKey, err := x509.ParsePKCS8PrivateKey(privateKeyData.Bytes); err != nil {
				panic(err)
			} else {
				if pkey, ok := rustorePrivateKey.(*rsa.PrivateKey); ok {
					now := time.Now().Format(time.RFC3339)
					digest := sha512.Sum512([]byte(fmt.Sprintf("%v%v", *keyId, now)))
					if signature, err := rsa.SignPKCS1v15(rand.Reader, pkey, crypto.SHA512, digest[:]); err != nil {
						panic(err)
					} else {
						bsignature := base64.RawStdEncoding.EncodeToString(signature)
						if bsignature != "" {
							r := rustoreApi.NewRequest()
							r.SetBody(map[string]string{"keyId": *keyId, "timestamp": now, "signature": bsignature})
							jweReply := new(RustoreJwtReply)
							r.SetResult(jweReply)
							if _, err := r.Post("https://public-api.rustore.ru/public/auth"); err != nil {
								panic(err)
							}
							if jweReply.Code != "OK" {
								panic(jweReply.Message)
							}
							jwe = jweReply.Body.Jwe
							versionReply := new(RustoreVersionReply)
							r = rustoreApi.NewRequest()
							r.SetResult(versionReply)
							r.SetBody(map[string]string{"publishType": *publishType})
							if _, err := r.Post(fmt.Sprintf("https://public-api.rustore.ru/public/v1/application/%v/version", *packageName)); err != nil {
								panic(err)
							}
							draftVersion = versionReply.Version
							fmt.Printf("Current draft version %v\n", draftVersion)
							for _, apk := range apks {
								uploadReply := new(RustoreCommonReply)
								r = rustoreApi.NewRequest()
								r.SetResult(uploadReply)
								r.SetFile("file", apk.Filename)
								fmt.Printf("Uploading %+v\n", apk)
								url := fmt.Sprintf("https://public-api.rustore.ru/public/v1/application/%v/version/%v/apk?servicesType=%v&isMainApk=%v", *packageName, draftVersion, apk.ServiceType, apk.IsMain)
								if apk.IsAAB {
									url = fmt.Sprintf("https://public-api.rustore.ru/public/v1/application/%v/version/%v/aab", *packageName, draftVersion)
								}
								if _, err := r.Post(url); err != nil {
									panic(err)
								}
							}
							fmt.Printf("All files uploaded. Commiting draft %v\n", draftVersion)
							commitReply := new(RustoreCommonReply)
							r = rustoreApi.NewRequest()
							r.SetResult(commitReply)
							if _, err := r.Post(fmt.Sprintf("https://public-api.rustore.ru/public/v1/application/%v/version/%v/commit", *packageName, draftVersion)); err != nil {
								panic(err)
							}
							fmt.Printf("Draft %v published\n", draftVersion)
						}
					}
				} else {
					panic("Invalid private key")
				}
			}
		}
	}
}
